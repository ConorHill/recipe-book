import { Ingredient } from './../../Shared/ingredient.model';
import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-shoppinglist-edit',
  templateUrl: './shoppinglist-edit.component.html',
  styleUrls: ['./shoppinglist-edit.component.css']
})
export class ShoppinglistEditComponent implements OnInit {
  @ViewChild('IngredientAmount') IngredientAmount: ElementRef;
  @ViewChild('IngredientName') IngredientName: ElementRef;
  @Output() ingredient = new EventEmitter<Ingredient>();
  constructor() { }

  ngOnInit() {
  }

  onAddItem() {
    const ingredientName = this.IngredientName.nativeElement.value;
    const ingredientAmount = this.IngredientAmount.nativeElement.value;
    const ingAdded = new Ingredient(ingredientName, ingredientAmount);
    this.ingredient.emit(ingAdded);
  }

}
