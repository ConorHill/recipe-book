import { Ingredient } from './../Shared/ingredient.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shoppinglist',
  templateUrl: './shoppinglist.component.html',
  styleUrls: ['./shoppinglist.component.css']
})
export class ShoppinglistComponent implements OnInit {
  ingredients = [
    new Ingredient('Apples', 3),
    new Ingredient('Butter', 50)
  ];

  constructor() { }

  ngOnInit() {
  }

  extractIngredient(thisIngredient: Ingredient) {
    this.ingredients.push(thisIngredient);
  }

}
